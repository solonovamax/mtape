package com.retrixe.mtape;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.retrixe.mtape.points.Point3d;
import com.retrixe.mtape.points.Point3i;
import com.retrixe.mtape.render.RenderBlockFrame;
import com.retrixe.mtape.render.RenderColor;
import com.retrixe.mtape.render.RenderLine;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.BackgroundRenderer;
import net.minecraft.client.render.DiffuseLighting;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.Vec3d;

public class MTapeListenerWorldRender {
    private float oldFog;
    private final MTape controller;
    private final RenderColor lookingAtColor = new RenderColor(0, 255, 0);
    private final RenderColor originColor = new RenderColor(255, 255, 0);
    private final RenderColor selectedColor = new RenderColor(255, 128, 0);
    private final RenderColor tapeLineColor = new RenderColor(255, 128, 0);

    public MTapeListenerWorldRender(MTape controller) {
        this.controller = controller;
    }

    public void onRender() {
        // No need to run GL commands, M-Tape isn't running.
        if (!this.controller.isTapeEnabled() && this.controller.getSelected().size() == 0) return;

        MinecraftClient mc = MinecraftClient.getInstance();
        if (mc.getCameraEntity() == null) return;
        MatrixStack matrixStack = RenderSystem.getModelViewStack();
        this.preRenderSetupGL(matrixStack, mc);

        Point3d lookingAt = this.controller.getLookingAt();
        Point3d originPos = this.controller.getOrigin();
        boolean lookingAtDrawn = false;
        boolean originDrawn = false;
        if (this.controller.isTapeEnabled()) {
            if (lookingAt != null) {
                RenderBlockFrame.setColor(this.lookingAtColor);
                RenderBlockFrame.setLineWidth(4.0D);
                RenderBlockFrame.render(lookingAt.x(), lookingAt.y(), lookingAt.z());
                lookingAtDrawn = true;
            }

            if (originPos != null) {
                RenderBlockFrame.setColor(this.originColor);
                RenderBlockFrame.setLineWidth(4.0D);
                RenderBlockFrame.render(originPos.x(), originPos.y(), originPos.z());
                originDrawn = true;
            }

            if (lookingAtDrawn && originDrawn) {
                RenderLine.setColor(this.tapeLineColor);
                RenderLine.setLineWidth(2.0D);
                RenderLine.render(
                        originPos.x() + 0.5D, originPos.y() + 0.5D, originPos.z() + 0.5D,
                        lookingAt.x() + 0.5D, lookingAt.y() + 0.5D, lookingAt.z() + 0.5D
                );
            }
        }

        RenderBlockFrame.setColor(this.selectedColor);
        RenderBlockFrame.setLineWidth(4.0D);

        for (Point3i point : this.controller.getSelected()) {
            RenderBlockFrame.render(point.x(), point.y(), point.z());
        }

        this.postRenderSetupGL(matrixStack);
    }

    private void preRenderSetupGL(MatrixStack matrixStack, MinecraftClient mc) {
        matrixStack.push();
        DiffuseLighting.disableForLevel(matrixStack.peek().getPositionMatrix());
        RenderSystem.disableCull();
        RenderSystem.enableBlend();
        RenderSystem.blendFuncSeparate(
                GlStateManager.SrcFactor.SRC_ALPHA,
                GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA,
                GlStateManager.SrcFactor.ONE,
                GlStateManager.DstFactor.ZERO
        );
        RenderSystem.disableTexture();
        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
        // RenderSystem.glMultiTexCoord2f(GL13.GL_TEXTURE1, 240.0F, 240.0F);
        RenderSystem.depthMask(false);
        oldFog = RenderSystem.getShaderFogStart();
        BackgroundRenderer.clearFog();
        RenderSystem.disableDepthTest();
        Vec3d cameraPos = mc.gameRenderer.getCamera().getPos();
        matrixStack.translate(-cameraPos.getX(), -cameraPos.getY(), -cameraPos.getZ());
        RenderSystem.applyModelViewMatrix();
    }

    private void postRenderSetupGL(MatrixStack matrixStack) {
        RenderSystem.setShaderFogStart(oldFog);
        RenderSystem.enableDepthTest();
        RenderSystem.depthMask(true);
        RenderSystem.enableTexture();
        RenderSystem.disableBlend();
        RenderSystem.enableCull();
        DiffuseLighting.enableForLevel(matrixStack.peek().getPositionMatrix());
        matrixStack.pop();
        RenderSystem.applyModelViewMatrix();
    }
}
