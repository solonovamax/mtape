package com.retrixe.mtape.render;

public class RenderColor {
    protected final byte red;
    protected final byte green;
    protected final byte blue;
    protected final byte alpha;

    public RenderColor(int red, int green, int blue) {
        this(red, green, blue, 255);
    }

    public RenderColor(int red, int green, int blue, int alpha) {
        this.red = (byte)red;
        this.green = (byte)green;
        this.blue = (byte)blue;
        this.alpha = (byte)alpha;
    }

    public byte red() {
        return this.red;
    }

    public byte green() {
        return this.green;
    }

    public byte blue() {
        return this.blue;
    }

    public byte alpha() {
        return this.alpha;
    }

    public int colorAsInt() {
        return this.red << 24 | this.green << 16 | this.blue;
    }
}
