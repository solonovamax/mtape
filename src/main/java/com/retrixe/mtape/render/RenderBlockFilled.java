package com.retrixe.mtape.render;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.client.render.VertexFormats;

public class RenderBlockFilled {
    private static byte red;
    private static byte green;
    private static byte blue;
    private static byte alpha;

    public static void setColor(RenderColor color) {
        red = color.red();
        green = color.green();
        blue = color.blue();
        alpha = color.alpha();
    }

    public static void setColor(RenderColor color, byte alpha) {
        red = color.red();
        green = color.green();
        blue = color.blue();
        RenderBlockFilled.alpha = alpha;
    }

    public static void render(double x0, double y0, double z0) {
        double x1 = x0 + 1.0D;
        double y1 = y0 + 1.0D;
        double z1 = z0 + 1.0D;
        Tessellator tess = Tessellator.getInstance();
        BufferBuilder renderer = tess.getBuffer();
        RenderSystem.setShader(GameRenderer::getRenderTypeLinesShader);
        renderer.begin(VertexFormat.DrawMode.TRIANGLE_FAN, VertexFormats.LINES);
        renderer.vertex(x0, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x0, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x0, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x0, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        tess.draw();
        renderer.begin(VertexFormat.DrawMode.TRIANGLE_FAN, VertexFormats.LINES);
        renderer.vertex(x1, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x0, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x0, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x0, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0).next();
        renderer.vertex(x1, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0).next();
        tess.draw();
    }
}
