package com.retrixe.mtape.mixin;

import com.retrixe.mtape.MTapeMod;
import net.minecraft.client.render.WorldRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(WorldRenderer.class)
public class PostRenderEntitiesMixin {
	@Inject(
			method = "render",
			at = @At(
					value = "INVOKE", ordinal = 1,
					target = "Lnet/minecraft/client/render/WorldRenderer;renderWeather(Lnet/minecraft/client/render/LightmapTextureManager;FDDD)V"
			)
	)
	private void onRenderWorldLastNormal(
			net.minecraft.client.util.math.MatrixStack matrices,
			float tickDelta, long limitTime, boolean renderBlockOutline,
			net.minecraft.client.render.Camera camera,
			net.minecraft.client.render.GameRenderer gameRenderer,
			net.minecraft.client.render.LightmapTextureManager lightmapTextureManager,
			net.minecraft.util.math.Matrix4f matrix4f,
			CallbackInfo ci) { MTapeMod.getInstance().onPostRenderEntities(); }

	@Inject(
			method = "render",
			slice = @Slice(
					from = @At(
							value = "FIELD", ordinal = 1, // start from the endDrawing() call
							target = "Lnet/minecraft/client/render/RenderPhase;WEATHER_TARGET:Lnet/minecraft/client/render/RenderPhase$Target;"
					),
					to = @At(
							value = "INVOKE", ordinal = 1, // end at the second renderWeather call
							target = "Lnet/minecraft/client/render/WorldRenderer;renderWeather(Lnet/minecraft/client/render/LightmapTextureManager;FDDD)V"
					)
			),
			at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gl/ShaderEffect;render(F)V")
	)
	private void onRenderWorldLastFabulous(
			net.minecraft.client.util.math.MatrixStack matrices,
			float tickDelta, long limitTime, boolean renderBlockOutline,
			net.minecraft.client.render.Camera camera,
			net.minecraft.client.render.GameRenderer gameRenderer,
			net.minecraft.client.render.LightmapTextureManager lightmapTextureManager,
			net.minecraft.util.math.Matrix4f matrix4f,
			CallbackInfo ci) { MTapeMod.getInstance().onPostRenderEntities(); }
}
