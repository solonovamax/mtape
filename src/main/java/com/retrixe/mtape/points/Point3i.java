package com.retrixe.mtape.points;

import java.util.Arrays;

public class Point3i {
    private final int x;
    private final int y;
    private final int z;

    public Point3i(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int x() {
        return this.x;
    }

    public int y() {
        return this.y;
    }

    public int z() {
        return this.z;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Point3i)) {
            return false;
        } else {
            Point3i point = (Point3i)obj;
            return this.x == point.x && this.y == point.y && this.z == point.z;
        }
    }

    public int hashCode() {
        return Arrays.hashCode(new int[]{this.x, this.y, this.z});
    }

    public String toString() {
        return "" + this.x + " " + this.y + " " + this.z;
    }

    public Point3d toPoint3d() {
        return new Point3d(this.x, this.y, this.z);
    }
}
